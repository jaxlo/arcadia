
fn main() {
    greeting();
}


fn greeting() {
    let banner = r###"
       db                                                  88 88             
      d88b                                                 88 ""             
     d8'`8b                                                88                
    d8'  `8b     8b,dPPYba,  ,adPPYba, ,adPPYYba,  ,adPPYb,88 88 ,adPPYYba,  
   d8YaaaaY8b    88P'   "Y8 a8"     "" ""     `Y8 a8"    `Y88 88 ""     `Y8  
  d8""""""""8b   88         8b         ,adPPPPP88 8b       88 88 ,adPPPPP88  
 d8'        `8b  88         "8a,   ,aa 88,    ,88 "8a,   ,d88 88 88,    ,88  
d8'          `8b 88          `"Ybbd8"' `"8bbdP"Y8  `"8bbdP"Y8 88 `"8bbdP"Y8
A project by Jackson Lohman"###;
    println!("{banner}");
}
